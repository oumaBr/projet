package com.example.projet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.projet.data.CeriRepository;
import com.example.projet.data.Item;

import java.util.List;

public class ListViewModel extends AndroidViewModel {

    private CeriRepository repository;
    private LiveData<List<Item>> allItems;

    public ListViewModel (Application application) {
        super(application);
        repository = CeriRepository.get(application);
        allItems = repository.getAllItems();
    }


    LiveData<List<Item>> getAllItems() {
        return allItems;
    }



    public Integer countItems(){
        return repository.countItems();
    }


    public void loadCollection() {
        repository.loadCollection();
    }



}
