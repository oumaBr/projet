    package com.example.projet.data;

    import android.app.Application;
    import android.util.Log;

    import androidx.lifecycle.LiveData;
    import androidx.lifecycle.MutableLiveData;

    import com.example.projet.data.database.CeriRoomDatabase;
    import com.example.projet.data.database.ItemDao;
    import com.example.projet.data.webservice.CERIInterface;

    import java.util.List;
    import java.util.Map;
    import java.util.concurrent.ExecutionException;
    import java.util.concurrent.Future;

    import retrofit2.Call;
    import retrofit2.Callback;
    import retrofit2.Response;
    import retrofit2.Retrofit;
    import retrofit2.converter.moshi.MoshiConverterFactory;

    import static com.example.projet.data.database.CeriRoomDatabase.databaseWriteExecutor;

    public class CeriRepository {
        private static final String TAG = CeriRepository.class.getSimpleName();

        private MutableLiveData<Item> selectedItem;


        private LiveData<List<Item>> allItems;

        private ItemDao itemDao;


        private CERIInterface api;


        Item item = new Item();

        private static volatile CeriRepository INSTANCE;


        public synchronized static CeriRepository get(Application application) {
            if (INSTANCE == null) {
                INSTANCE = new CeriRepository(application);
            }

            return INSTANCE;
        }

        public CeriRepository(Application application) {
            CeriRoomDatabase db = CeriRoomDatabase.getDatabase(application);
            itemDao = db.ItemDao();

            allItems=itemDao.getAllItems();
            selectedItem = new MutableLiveData<>();
            Retrofit retrofit =
                    new Retrofit.Builder()
                            .baseUrl("https://demo-lia.univ-avignon.fr")
                            .addConverterFactory(MoshiConverterFactory.create())
                            .build();

            api = retrofit.create(CERIInterface.class);

        }

        public void loadCollection() {
            api.getCollection().enqueue(new Callback<Map<String, ItemResponse>>() {
                @Override
                public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {

                    for (Map.Entry<String,ItemResponse> items1: response.body().entrySet()) {
                        String key = items1.getKey();
                        ItemResponse itemDetail = items1.getValue();
                        ItemResult.transferInfo(itemDetail,item);
                        item.setItemID(key);
                        insertItem(item);

                    }
                }

                @Override
                public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                    Log.d("app", "Error get collection :" + t.getMessage());
                }
            });

        }

        public MutableLiveData<Item> getSelectedItem() {
            return selectedItem;
        }
        public LiveData<List<Item>> getAllItems() {
            return allItems;
        }

        public long insertItem(Item newitem) {
            Future<Long> flong = databaseWriteExecutor.submit(() -> itemDao.insertItem(newitem));
            long res = -1;
            try {
                res = flong.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return res;
        }

        public Integer countItems() {
            Future<Integer> flong= databaseWriteExecutor.submit(()->itemDao.countItems());
            int res=-1;
            try {
                res = flong.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return res;
        }

        public void getItem(long id)  {
            Future<Item> fitem = databaseWriteExecutor.submit(() -> {
                return itemDao.getItem(id);
            });
            try {
                selectedItem.setValue(fitem.get());
                Log.d("app,",""+fitem.get());
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void deleteAllItems() {
            databaseWriteExecutor.execute(() -> {
                itemDao.deleteAllItems();
            });
        }
    }
